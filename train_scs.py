﻿# Keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding, Lambda
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint
import tensorflow.keras.backend as K
# SentencePiece
import sentencepiece as spm
# Scipy
import numpy as np
# General
from tqdm import tqdm
import json
import os

# Settings
max_seq_length = 50
batch_size = 2048
epochs = 1000

# Load Tokenizer
sp = spm.SentencePieceProcessor()
sp.Load("tokenizer/english.model")
vocab_size = len(sp)

# Init training
x, y = [], []

def encode(text):
	array = np.zeros(max_seq_length)
	toks = sp.EncodeAsIds(text)
	array[:len(toks)] = toks[:max_seq_length]
	return array

# Load Data
with open('/run/media/kyle/Rudolph/Amazon/Reviews/train.json') as source:
	for text in tqdm(source):
		try:
			data = json.loads(text)
			x.append(encode(data['text']))
			y.append((data['score'] * 2) - 1)
		except Exception as e:
			print(e)

x = np.array(x)
y = np.array(y)

# Build Model
model = Sequential()
model.add(Embedding(vocab_size, 1, mask_zero=True))
model.add(Lambda(lambda x: K.mean(x, axis=1)))

model.compile(loss='mse', optimizer='adam')

model.summary()

# Train Model
model.fit(
	x, y,
	batch_size=batch_size,
	epochs=epochs,
	validation_split=0.10,
	callbacks=[
		TensorBoard(log_dir='logs'),
		ModelCheckpoint('checkpoint.h5')
	],
)

model.save('sentinet_scs.h5')
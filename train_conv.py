﻿# Keras
from keras.models import Sequential
from keras.layers import Dense, Embedding, Conv1D, Flatten
from keras.utils import to_categorical
from keras.callbacks import TensorBoard, ModelCheckpoint
# SentencePiece
import sentencepiece as spm
# Scipy
import numpy as np
# General
from tqdm import tqdm
import json
import os

# Settings
max_seq_length = 50
vocab_size = 36000
batch_size = 128
epochs = 5

# Load Tokenizer
sp = spm.SentencePieceProcessor()
sp.Load("tokenizer/english.model")

# Init training
x, y = [], []

def encode(text):
	array = np.zeros(max_seq_length)
	toks = sp.EncodeAsIds(text)
	array[:len(toks)] = toks[:max_seq_length]
	return array

# Load Data
with open('/media/kyle/Rudolph/Amazon/Reviews/train.json') as source:
	for text in tqdm(source):
		try:
			data = json.loads(text)
			x.append(encode(data['text']))
			y.append(data['score'])
		except Exception as e:
			print(e)

x = np.array(x)
y = np.array(y)

# Build Model
model = Sequential()
model.add(Embedding(vocab_size, 64, input_length=max_seq_length))
model.add(Conv1D(512, 3))
model.add(Flatten())
model.add(Dense(1))

model.compile(loss='mse', optimizer='adam')

# Train Model
model.fit(
	x, y,
	batch_size=batch_size,
	epochs=epochs,
	validation_split=0.10,
	callbacks=[
		TensorBoard(log_dir='logs'),
		ModelCheckpoint('checkpoint.model')
	],
)

model.save('sentinet_cnn.model')